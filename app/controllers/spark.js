module.exports = function(router) {
  require('tropo-webapi-node');
  var sparkCreateRoom = require('./sparkCreateRoom');
  var sparkDeleteRoom = require('./sparkDeleteRoom');
  var sparkSendMessage = require('./sparkSendMessage');
  var roomId = null;
  var sparkMessage = null;

  //researcher participant web session has started
  router.get('/session/create', function(req,res,next) {
  //router.post('/session/create', function(req,res,next) {
    //it("should tell everyone in the chat the url of the call that started and any associated information")
      //sql generates the id
      //generate the url from the id
      //http post url to the room using spark
    //console.log('/session/create req',req);
    sparkCreateRoom({title: 'UAT Session 1234'}).then(function(data){
      /*Store the res data?*/
      console.log('store spark response', data);
      roomId = data.id;
      res.send(data);

      sparkMessage  = "Realtime Transcript of UAT Session 1234 available at http://a3096843.ngrok.io/workspace/1234/observer";
      sparkSendMessage(roomId,sparkMessage).then(function(data){
        /*Store the res data?*/
        console.log('send message spark response', data);
        res.send(data);
        next();
      });
    });
  });

  //researcher participant web session has ended
  router.get('/session/remove', function(req,res,next) {
    //it("should tell everyone in the chat the url of the call that started and any associated information")
      //sql generates the id
      //generate the url from the id
      //http post url to the room using spark
    sparkDeleteRoom(roomId).then(function(data){
      /*Store the res data?*/
      console.log('delete spark response', data);
      res.send(data);
      next();
    });
  });

  //researcher participant web session has ended
  router.get('/session/message', function(req,res,next) {
    console.log(req.body);
    //it("should tell everyone in the chat the url of the call that started and any associated information")
      //sql generates the id
      //generate the url from the id
      //http post url to the room using spark
    //TODO: NICK!!!! I need to send this up immediately for review. We'll have to make this message dynamic.
    sparkMessage  = req.query.query || "Realtime Transcript of UAT Session 1234 available at http://a3096843.ngrok.io/workspace/1234/observer";

    sparkSendMessage(roomId,sparkMessage).then(function(data){
      /*Store the res data?*/
      console.log('send message spark response', data);
      res.send(data);
      next();
    });
  });
};
