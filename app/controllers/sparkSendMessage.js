module.exports = function(roomId,msg) {
    var apiKeys = require('../secret/apiKeys.js');
    var Spark = require('csco-spark');
    var spark = Spark({
        uri: 'https://api.ciscospark.com/v1',
        token: apiKeys.sparkKey()
    });

    return  spark.sendMessage({
            roomId: roomId,
            text: msg
        });


    // send message to room
    // delete room

    //router.post('https://api.ciscospark.com/v1/rooms',function(req,res,next){
    //    req.set({
    //        'Authentication': "Bearer " + apiKeys.sparkKey(),
    //        'Content-Type': 'application/json'
    //    });
    //    req.body = {
    //        "title" : "Project Unicorn - Sprint 0"
    //    };
    //    req.send();
    //    console.log(res.body);
    //    //res.render();
    //    next();
    //
    //    // Send a Message
    //    spark.sendMessage({
    //        roomId:'roomId',
    //        text: 'message'
    //    }).then(function(res){
    //        /*Store the res data?*/
    //    });
    //
    //    // Create a Spark Room
    //    spark.createRoom({ title: 'title'}).then(function(res){
    //        /*Store the res data?*/
    //    });
    //
    //    // Delete a Room
    //    spark.removeRoom(roomId).then(function(res){
    //        /* resp should be null */
    //    });
    //
    //});

    //// Getting Spark Rooms
    //var listRooms = spark.listItemEvt({
    //    item: 'rooms',
    //    max: '15' || undefined // Default = 50
    //});
    //
    //// Listen for Rooms
    //listRooms.on('rooms', function(rooms) {
    //    console.log('rooms',rooms)
    //});
    //
    //listRooms.on('rooms-end', function(rooms) {
    //    // Yes I am sending Data on the End Event
    //    // I believe most don't
    //})
    //
    //listRooms.emit('rooms');
    //
};
